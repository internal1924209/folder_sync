import argparse
import logging
import os
import shutil
import sched
import time


def synchronize_folders(source_folder, replica_folder, log_file):
    logging.basicConfig(
        filename=log_file, level=logging.INFO, format="%(asctime)s %(message)s"
    )
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logging.getLogger("").addHandler(console)

    for root, dirs, files in os.walk(source_folder):
        for file in files:
            source_file = os.path.join(root, file)
            replica_file = os.path.join(
                replica_folder, os.path.relpath(source_file, source_folder)
            )

            if os.path.exists(replica_file):
                source_stat = os.stat(source_file)
                replica_stat = os.stat(replica_file)

                if source_stat.st_mtime > replica_stat.st_mtime:
                    shutil.copy2(source_file, replica_file)
                    logging.info(f"Copied file: {source_file} -> {replica_file}")
            else:
                shutil.copy2(source_file, replica_file)
                logging.info(f"Copied file: {source_file} -> {replica_file}")

    for root, dirs, files in os.walk(replica_folder):
        for file in files:
            replica_file = os.path.join(root, file)
            source_file = os.path.join(
                source_folder, os.path.relpath(replica_file, replica_folder)
            )

            if not os.path.exists(source_file):
                os.remove(replica_file)
                logging.info(f"Removed file: {replica_file}")


def schedule_sync(scheduler, interval, source_folder, replica_folder, log_file):
    synchronize_folders(source_folder, replica_folder, log_file)
    scheduler.enter(
        interval,
        1,
        schedule_sync,
        (scheduler, interval, source_folder, replica_folder, log_file),
    )


def main():
    parser = argparse.ArgumentParser(description="Folder synchronization program")
    parser.add_argument("source_folder", help="Path to the source folder")
    parser.add_argument("replica_folder", help="Path to the replica folder")
    parser.add_argument(
        "sync_interval", type=int, help="Synchronization interval in seconds"
    )
    parser.add_argument("log_file", help="Path to the log file")
    args = parser.parse_args()

    scheduler = sched.scheduler(time.time, time.sleep)
    scheduler.enter(
        0,
        1,
        schedule_sync,
        (
            scheduler,
            args.sync_interval,
            args.source_folder,
            args.replica_folder,
            args.log_file,
        ),
    )

    scheduler.run()


if __name__ == "__main__":
    main()
