# folder_sync

# Description:
- Simple python program that syncs two folders 

# Command line:
- poetry run python folder_sync.py /path/to/source_folder /path/to/replica_folder 60 /path/to/log_file.log

- This command starts the folder synchronization program using Poetry and Python. The program will synchronize the contents of the source folder with the replica folder. Synchronization will be performed periodically every 60 seconds. File creation, copying, and removal operations will be logged to the specified log file.

# Command Arguments:

- /path/to/source_folder: The path to the source folder that you want to synchronize.
- /path/to/replica_folder: The path to the replica folder where the synchronized content will be stored.
- 60: The synchronization interval in seconds. The program will synchronize the folders every 60 seconds.
- /path/to/log_file.log: The path to the log file where file operations will be logged.
